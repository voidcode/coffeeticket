package service_shared

import (
	"context"
	"fmt"
)

func DBDoctor(schema string) bool {


	conn := Conn
	CheckErr(ConErr)

	var bg = context.Background()

	if schema != "" {
		_, err := conn.Exec(bg, schema)
		CheckErr(err)

		fmt.Println("DB checked successfully.")
	}
	return true

}
