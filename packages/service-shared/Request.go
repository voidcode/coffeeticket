package service_shared

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"math/rand"
	"net/http"
	"os"
	"time"
)

var Conn, ConErr = pgxpool.Connect(context.Background(), os.Getenv("DB_URL"))
var ConnAdmin, ConnErr = pgxpool.Connect(context.Background(), os.Getenv("DB_URL_ADMIN"))
var LetterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func GenerateToken(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = LetterRunes[rand.Intn(len(LetterRunes))]
	}
	return string(b)
}

type BoolResult struct {
	Result bool `json:"result"`
}

type PromoCode struct {
	Type           string    `json:"type"`
	Code           string    `json:"code"`
	ValidTime      time.Time `json:"valid_time"`
	Mount          int       `json:"mount"`
	OrganizationId string    `json:"organization_id"`
}

type StringResult struct {
	Result string `json:"result"`
}

type ErrorResult struct {
	Error string `json:"error"`
}

type UserToken struct {
	Token string `json:"token"`
}

func RequestHandler(request func(req *http.Request) (interface{}, error)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Content-Type", "application/json")

		result, err := request(req)

		if err != nil {
			fmt.Println(err)
			result := ErrorResult{Error: err.Error()}
			jsonData, _ := json.Marshal(result)
			w.Write(jsonData)
			return
		}

		jsonData, err := json.Marshal(result)
		w.Write(jsonData)
	}
}
