package service_shared

import (
	"net/http"
	"net/http/httptest"
	"testing"
)


func RequestResult(request *http.Request, function func(r *http.Request) (interface{}, error)) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(RequestHandler(function))
	handler.ServeHTTP(rr, request)
	return rr
}

func TestGetRequest(t *testing.T, url string, request func(r *http.Request) (interface{}, error), expected string) {
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := RequestResult(req, request)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}