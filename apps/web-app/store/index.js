import {signInWithRedirect, signOut } from 'firebase/auth'


export const actions = {
  async getPlaces(state, id) {
    const url = id ? `/places/${id}` : '/places'
    return await this.$axios.$get(url)
  },
  async getPlaceWithUID(state, { id, uid }) {
    const url = `http://localhost:8090/places/${id}`;

    return await fetch(url, {
      method: "POST",
      body: JSON.stringify({
        "device_id": uid
      })
    }).then(result => result.json())

  },
  setUser(state, user) {
    // console.log("Action!")
    state.commit('SET_USER', user)
  },
  async signIn(state, { auth, provider }) {
    return await signInWithRedirect(auth, provider)
  },
  async signOut(state, auth) {
    state.commit("RESET_USER")
    return await signOut(auth)
  }
}
export const state = () => ({
  user: null
})

export const getters = {
  getUser: state => {
    return state.user || { }
  },
  isLoggedIn: state => {
    return state.user != null
  }
}

export const mutations = {
  SET_USER: (state, user) => {
    const unwrappedUser = Object.freeze(user)
    state.user = unwrappedUser
  },
  RESET_USER: (state) => {
    state.user = null
  }
}

