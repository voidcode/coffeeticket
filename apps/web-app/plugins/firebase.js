import {initializeApp, getApps} from "firebase/app";
import {getAnalytics, logEvent} from "firebase/analytics";
import { getAuth } from 'firebase/auth'

export default ({ store }) => {

  const firebaseConfig = {
    apiKey: "AIzaSyDC1eVbIW6o19IkFjHp5vZdswfTqPeV-QU",
    authDomain: "bonusticket.firebaseapp.com",
    projectId: "bonusticket",
    storageBucket: "bonusticket.appspot.com",
    messagingSenderId: "427080339514",
    appId: "1:427080339514:web:7155b1c24c4494a30f2503",
    measurementId: "G-SDKYVT68T1"
  }

  const getApp = () => {
    const apps = getApps()
    if (!apps.length) {
      return initializeApp(firebaseConfig)
    } else {
      return apps[0]
    }
  }

  const app = getApp();

  const auth = getAuth()
  auth.onAuthStateChanged((user) => {
    if (user) {
      // has user
      console.log("Saved user!")
      store.dispatch('setUser', user);
    }
  })


  const logError = (description, fatal) => {
    if (process.browser) {
      const analytics = getAnalytics(app)
      logEvent(analytics, 'exception', {
        description,
        fatal
      })
      console.log("Error event sent!")
    }
  }
  return { app, logError }
}
