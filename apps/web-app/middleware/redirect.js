export default function ({ route, redirect, store }) {
  // Список с переадресациями
  const redirects = [
    {
      from: /^\/$/,
      to: "/place"
    },
    {
      from: /^\/ticket\/.+$/,
      to: "/login"
    }
  ]

  const redirectItem = redirects.find((r) => route.path.match(r.from))

  if (redirectItem) {
    if (redirectItem === redirects[1] && store.getters.isLoggedIn) {
      return
    }
   redirect(redirectItem.to)
  }
}
