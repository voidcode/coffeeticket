//
//  AddSafeArea.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

enum SafeAreaEdge {
    case bottom
    case top
    case all
}

extension View {
    func addSafeArea<T: View>(background: T, edges: SafeAreaEdge = .all) -> some View {
        var topPadding: CGFloat = 0
        var bottomPadding: CGFloat = 0
        
        switch edges {
            case .bottom:
                bottomPadding = 20
            case .top:
                topPadding = 44
            case .all:
                topPadding = 44
                bottomPadding = 20
        }
        
        return self.padding(.top, topPadding).padding(.bottom, bottomPadding).background(background)
    }
}

