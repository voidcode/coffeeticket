//
//  Expand.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

enum Direction {
    case vertical
    case horizontal
    case all
}

extension View {
    func expand(_ direction: Direction = .all) -> some View {
        switch direction {
            case .horizontal:
                return self.frame(maxWidth: .infinity)
            case .vertical:
                return self.frame(maxHeight: .infinity)
            case .all:
                return self.frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

