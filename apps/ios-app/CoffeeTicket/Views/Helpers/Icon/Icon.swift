//
//  Icon.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

extension Image {
    func icon(_ color: Color) -> some View {
        self
            .resizable()
            .scaledToFit()
            .frame(width: 18, height: 18)
            .foregroundColor(color)
    }
}
