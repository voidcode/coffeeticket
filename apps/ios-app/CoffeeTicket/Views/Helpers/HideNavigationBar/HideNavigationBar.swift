//
//  HideNavigationBar.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

extension View {
    func hideNavigationBar() -> some View {
        self.navigationBarHidden(true)
    }
}

