//
//  MainScreen.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI
import SwiftExtended

struct MainScreen: View {
        
    
    
    init() {
        UINavigationBar.configureNavigationBar()
    }
    
    
    var body: some View {
        
        NavigationView {
            
        VStack {
            Text("Здесь будет камера").expand(.all).background(Color.black)
        }
        .expand(.all)
        .edgesIgnoringSafeArea(.bottom)
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle(Text("Наведите камеру"))
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarTrailing) {
            NavigationButton(destination: RecentScreen(), action: {true}) {

                Image(systemName: "clock").icon(.white)

            }
            }
            
        })
        
        }
        .edgesIgnoringSafeArea(.all)
        
    }
    
    
    
    // Navigation bar
//    private struct NavigationBar: View {
//        var body: some View {
//            ZStack {
//                HStack {
//                    Spacer()
//                    Text("Наведите камеру").font(.system(size: 18)).foregroundColor(.white)
//                    Spacer()
//                }
//                HStack {
//                    Spacer()
//                    NavigationButton(destination: RecentScreen(), action: {true}) {
//
//                        Image(systemName: "clock").icon(.white)
//
//                    }
//
//                }.padding(.horizontal, 20)
//            }
//            .frame(height: 50)
//            .expand(.horizontal)
//
//        }
//    }
    
    

    
}
