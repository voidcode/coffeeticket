//
//  CafeItem.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

struct Cafe: Identifiable {
    var id = UUID()
    var name: String
    var place = "метро Первомайская"
}


struct CafeItem: View {
    
    let cafe: Cafe
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                
                
            Text("\(cafe.name)")
                .font(.system(size: 18,weight: .medium))
                .foregroundColor(.white)
                .padding(.top, 20)
            Spacer()
            
            Text("\(cafe.place)")
                .font(.system(size: 14))
                    .foregroundColor(.white)
                .padding(.bottom, 15)
                
            }
            .expand(.vertical)
            .padding(.horizontal, 20)
            Spacer()
            VStack {
                Spacer()
                Image(systemName: "chevron.right").icon(.white)
                Spacer()
            }.padding(.trailing, 20)
        }
        .expand(.horizontal)
        .frame(height: 80)
        .background(Color.greyOpaque)
        .cornerRadius(10)
        .padding(.horizontal, 15)
    }
}

struct CafeItem_Preview: PreviewProvider {
    static let cafe = Cafe(name: "Кофейня №1")
    static var previews: some View {
        VStack {
        CafeItem(cafe: cafe)
        }
        .expand()
        .background(Color.darkChocolate)
            
    }
}
