//
//  RecentScreen.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

struct RecentScreen: View {
    
    let cafes: [Cafe] = [
        Cafe(name: "Кофейня №1"),
        Cafe(name: "Кофейня №2"),
        Cafe(name: "Кофейня №3"),
        Cafe(name: "Кофейня №4"),

    
    
    ]
    
    
    
    
    init() {
        UINavigationBar.configureNavigationBar()
      }

    
    var body: some View {
        VStack {
            ScrollView {
                LazyVStack(spacing: 20) {
                    ForEach(cafes) { cafe in
                        CafeItem(cafe: cafe)
                    }
                }.padding(.vertical, 20)
            }
        }
        .expand()
        .background(Color.darkChocolate)
        .navigationTitle(Text("Недавно посещенные"))
        .navigationBarTitleDisplayMode(.inline)
        .edgesIgnoringSafeArea(.bottom)
    }
    
    
    
}


struct RecentScreen_Preview: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RecentScreen()
        }
    }
}
