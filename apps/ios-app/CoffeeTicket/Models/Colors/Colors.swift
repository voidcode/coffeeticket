//
//  Colors.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import Foundation
import SwiftUI

extension Color {
    static let chocolate: Color = Color("color_chocolate")
    static let darkChocolate = Color("color_chocolate_dark")
    static let greyOpaque = Color("color_grey_opaque")
}
