//
//  CoffeeTicketApp.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import SwiftUI

@main
struct CoffeeTicketApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
//                .preferredColorScheme(.dark)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
