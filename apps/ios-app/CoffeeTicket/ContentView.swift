//
//  ContentView.swift
//  CoffeeTicket
//
//  Created by Vlad Valkov on 08.09.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainScreen()
        .edgesIgnoringSafeArea(.all)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
