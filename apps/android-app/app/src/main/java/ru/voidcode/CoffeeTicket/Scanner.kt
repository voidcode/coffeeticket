package ru.voidcode.CoffeeTicket

import android.app.Activity
import android.os.Handler
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView


@Composable
fun Scanner(callback: (String) -> Unit) {
    val context = LocalContext.current
    var scanFlag by remember {
        mutableStateOf(false)
    }

    val compoundBarcodeView = remember {
        CompoundBarcodeView(context).apply {
            val capture = CaptureManager(context as Activity, this)
            capture.initializeFromIntent(context.intent, null)
            this.setStatusText("")
            this.resume()
            capture.decode()
            this.decodeContinuous { result ->
                if(scanFlag){
                    return@decodeContinuous
                }
                scanFlag = true
                result.text?.let { barCodeOrQr->
                    //Do something
                    Log.d("Tag", barCodeOrQr)
                    callback(barCodeOrQr)
                Handler().postDelayed({
                    scanFlag = false
                }, 5000)
                }
//                scanFlag = false

            }
        }
    }

    AndroidView(
        modifier = Modifier.background(Color.Red),
        factory = { compoundBarcodeView },
    )
}