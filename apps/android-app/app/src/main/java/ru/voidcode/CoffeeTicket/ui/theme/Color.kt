package ru.voidcode.CoffeeTicket.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Chocolate = Color(0xFF825555)
var DarkChocolate = Color(0xFF5C3C3C)
var GreyOpaque = Color(0x40FFFFFF)