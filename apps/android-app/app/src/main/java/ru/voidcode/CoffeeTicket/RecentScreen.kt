package ru.voidcode.CoffeeTicket

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.Place
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import java.util.*


data class Cafe(
    val name: String,
    ) {
    val id: UUID = UUID(16, 16)
}

@Composable
fun RecentScreen(navController: NavController?) {

    val cafes = mutableListOf<Cafe>().apply {
        repeat(1000){ this.add(element = Cafe("Cafe ($it)")) }
    }
    val listState = rememberLazyListState()

    val scrollState = rememberScrollState()


    Surface(color = MaterialTheme.colors.background)  {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            NavigationBar(
                title = { Text("Недавно посещенные") },
                trailing = { },
                leading = { IconButton(onClick = { navController?.popBackStack() }) {
                    Icon(Icons.Filled.ArrowBack, "go back")
                } }
            )
            LazyColumn(state = listState, modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.secondary)) {
                items(cafes) { cafe ->
                    CafeItem(navController = navController, cafe = cafe)
                }
            }
        }
    }
}

@Preview
@Composable
fun RecentScreen_Preview() {
    RecentScreen(navController = null)
}
