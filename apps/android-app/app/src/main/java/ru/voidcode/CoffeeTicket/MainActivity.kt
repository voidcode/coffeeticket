package ru.voidcode.CoffeeTicket

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import ru.voidcode.CoffeeTicket.ui.theme.CoffeeTicketTheme

class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        setContent {
            CoffeeTicketTheme {
                val systemUiController = rememberSystemUiController()

                systemUiController.setSystemBarsColor(
                    color = MaterialTheme.colors.primary,
                )
                var context = LocalContext.current
                CountPreferences(context = context)


                val navController = rememberNavController()
                // A surface container using the 'background' color from the theme
                NavHost(
                    navController = navController,
                    startDestination = "main"


                ) {
                    composable("main") { MainScreen(navController = navController) }
                    composable("recent") {
                       RecentScreen(navController = navController)
                    }
                    composable("cafe/{cafeId}") { backStackEntry ->
                        CafeScreen(navController, backStackEntry.arguments?.getString("cafeId"))
                    }


                }
            }
        }
    }
}
