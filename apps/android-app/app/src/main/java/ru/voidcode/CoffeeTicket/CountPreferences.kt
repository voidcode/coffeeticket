package ru.voidcode.CoffeeTicket

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class CountPreferences(private val context: Context) {

        companion object {
            lateinit var instance: CountPreferences
        }
        init {
            instance = this
        }



        val Context.dataStore : DataStore<Preferences> by preferencesDataStore(
            name = "count"
        )



        suspend fun save(id: String?, count: Int) {

            if (id == null) {
                return
            }

            var key = intPreferencesKey(id)
            context.dataStore.edit { preferences ->
                preferences[key] = count
            }
        }

    fun get(id: String?): Flow<Int> {

            var key = intPreferencesKey(id.toString())

            val result: Flow<Int> = context.dataStore.data.map { preferences ->
                preferences[key] ?: 0
            }
            return result
        }

}