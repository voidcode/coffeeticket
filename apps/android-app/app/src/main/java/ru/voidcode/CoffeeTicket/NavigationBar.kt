package ru.voidcode.CoffeeTicket

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Place
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun NavigationBar(title: @Composable() () -> Unit, trailing: @Composable() () -> Unit, leading: @Composable() () -> Unit) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .height(50.dp)
        .background(MaterialTheme.colors.primary)
        .padding(10.dp)) {
        Row {
            Spacer(modifier = Modifier.weight(1f))
            trailing()
        }
        Row {
            leading()
            Spacer(modifier = Modifier.weight(1f))
        }

        Row {
            Spacer(modifier = Modifier.weight(1f))
            title()
            Spacer(modifier = Modifier.weight(1f))

        }

    }
}