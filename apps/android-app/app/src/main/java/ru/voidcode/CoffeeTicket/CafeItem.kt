package ru.voidcode.CoffeeTicket

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable
fun CafeItem(navController: NavController?, cafe: Cafe) {

    Row(modifier = Modifier
        .padding(horizontal = 20.dp, vertical = 5.dp)
        .clip(RoundedCornerShape(size = 20.dp))
        .background(MaterialTheme.colors.onSecondary)
        .height(80.dp)
        .fillMaxWidth().clickable {
            navController?.navigate("cafe/${cafe.id}")
        }

    ) {
        Column(
            modifier = Modifier.fillMaxHeight(),
            verticalArrangement = Arrangement.Center
        ) {
            Text(text = cafe.name, modifier = Modifier.padding(start = 30.dp, top = 16.dp, ))
            Spacer(modifier = Modifier.weight(1f))
            Text(text = cafe.name, modifier = Modifier.padding(start = 30.dp, bottom = 19.dp))
        }
        Spacer(modifier = Modifier.weight(1f))
    }
}