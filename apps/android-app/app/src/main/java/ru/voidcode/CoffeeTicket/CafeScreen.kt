package ru.voidcode.CoffeeTicket

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.NavController
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


@Composable
fun CafeScreen(navController: NavController, cafeId: String?) {



    val preferences = CountPreferences.instance

    val count: State<Int> = preferences.get(id = "0").collectAsState(initial = 0)

    



    Surface(color = MaterialTheme.colors.secondary)  {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            NavigationBar(
                title = { /*TODO*/ },
                trailing = {},
                leading = { IconButton(onClick = { navController?.popBackStack() }) {
                    Icon(Icons.Filled.ArrowBack, "go back")
                }})

            Text(text = "${count.value}", fontSize = 20.sp)

            Button(onClick = {



                val preferences = CountPreferences.instance
                runBlocking {
                    launch {

                       var flow = preferences.get("0").take(1)
                        flow.collect { value ->
                            preferences.save(id = "0", count = value + 1)
                        }
                    }
                }

            }) {
                    Text("Increase")

            }

          }



    }

}