package ru.voidcode.CoffeeTicket

import android.content.res.Resources
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.Button
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Place
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import java.util.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


@Composable
fun MainScreen(navController: NavController?) {

    var scanned by remember { mutableStateOf(" ") }

    Surface(color = MaterialTheme.colors.background) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
        ) {
            NavigationBar(
                title = { Text("Наведите камеру") },
                trailing = { IconButton(onClick = { navController?.navigate("recent") }) {
                    Icon(Icons.Outlined.Place, "recent")
                } },
                leading = {}
            )

            Box {

                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Scanner(callback = {
                        scanned = it

                        val preferences = CountPreferences.instance
                        runBlocking {
                            launch {

                                var flow = preferences.get("0").take(1)
                                flow.collect { value ->
                                    preferences.save(id = "0", count = value + 1)
                                }
                            }
                        }
                    }

//
                    )
                }

                Snackbar {
                    Text("${scanned}")
                }

            }


        }
    }
}

@Preview
@Composable
fun MainScreen_Preview() {
    MainScreen(navController = null)
}
