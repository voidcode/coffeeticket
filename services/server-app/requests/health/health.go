package health

import (
	"net/http"
	"voidcode.ru/bonusticket/packages/service-shared"
)



func Health(req *http.Request) (interface{}, error) {
	result := service_shared.StringResult{ Result: "OK" }
	return result, nil
}
