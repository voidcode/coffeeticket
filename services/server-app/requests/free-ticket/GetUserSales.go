package freeTicketRequests

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)

type UserData struct {
	UserID string `json:"user_id"`
}

type ReturnedSale struct {
	SaleId         string    `json:"sale_id"`
	UserId         string    `json:"user_id"`
	Percent        string    `json:"percent"`
	OrganizationId string    `json:"organization_id"`
	ValidTime      time.Time `json:"valid_time"`
}

func GetUserSales(req *http.Request) (interface{}, error) {
	var userData UserData

	err := json.NewDecoder(req.Body).Decode(userData)

	shared.CheckErr(err)

	entries := findSalesByUserID(userData.UserID)
	if entries != nil {
		return entries, err
	} else {
		return shared.ErrorResult{"Organizations_Not_Found"}, err
	}
}

func findSalesByUserID(userID string) []ReturnedSale {
	rows, _ := Conn.Query(context.Background(), "SELECT * FROM sales WHERE user_id=$1", userID)
	var Sales []ReturnedSale

	for rows.Next() {
		var Sale ReturnedSale
		err := rows.Scan(&Sale.SaleId, &Sale.UserId, &Sale.Percent, &Sale.OrganizationId, &Sale.ValidTime)

		if Sale.SaleId == userID {
			if timeChecker(Sale.ValidTime) {
				Sales = append(Sales, Sale)
			}
		}
		shared.CheckErr(err)
	}
	return Sales
}

func timeChecker(validTime time.Time) bool {
	var timeToday time.Time
	timeToday = time.Now()

	if validTime.Before(timeToday) {
		return true
	} else {
		return false
	}
}
