package freeTicketRequests

import (
	"voidcode.ru/bonusticket/packages/service-shared"
)

var Conn = service_shared.Conn
var CheckErr = service_shared.CheckErr
var RandID = service_shared.GenerateToken
type ResultData = service_shared.BoolResult