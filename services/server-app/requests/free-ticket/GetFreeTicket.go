package freeTicketRequests

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"net/http"
	"time"
)

type TicketReq struct{
	DeviceId       string `json:"device_id"`
	OrganizationId string `json:"organization_id"`
}

type GetTicketRes struct{
	FreeTicketId string `json:"free_ticket_id"`
}


func GetFreeTicket(req *http.Request) (interface{}, error) {
	var ticketReq TicketReq
	var userId string
	var freeTicketId string
	var organizationId string

	err := json.NewDecoder(req.Body).Decode(&ticketReq)

	if err != nil {
		return nil, err
	}

	if findFreeTicket(ticketReq) {
		err := Conn.QueryRow(context.Background(),
			"SELECT * FROM free_tickets WHERE user_id=$1 AND organization_id=$2",
			ticketReq.DeviceId, ticketReq.OrganizationId).Scan(&userId, &freeTicketId, &organizationId)

		if err != nil {
			return nil, err
		}

		row := Conn.QueryRow(context.Background(),
			"SELECT * FROM recent WHERE user_id=$1 AND organization_id=$2",
			ticketReq.DeviceId, ticketReq.OrganizationId)

		var userId string
		var organizationId string
		var checkTime time.Time

		err = row.Scan(&userId, &organizationId, &checkTime)

		if err == pgx.ErrNoRows {
			// Нет использований free-ticket
			result := GetTicketRes{ FreeTicketId: freeTicketId }
			return result, nil
		}

		if err != nil {
			return nil, err
		}


		then := time.Now().Add(-24 * time.Hour)

		fmt.Println(then.Hour())
		fmt.Println(time.Now().Hour())

		//if time.Now().Hour() <= then.Hour() {
		//	return nil, errors.New("not enough time has elapsed")
		//} else {
			result := GetTicketRes{ FreeTicketId: freeTicketId }
			return result, nil
		//}
	} else if checkCount(ticketReq.DeviceId, ticketReq.OrganizationId){

		result := GetTicketRes{ FreeTicketId: generateTicket(ticketReq.DeviceId, ticketReq.OrganizationId) }

		return result, nil
	} else {
		return nil, errors.New("not enough tickets")
	}
}




func generateTicket(userId string, organizationId string) string {
	generated := RandID(16)

	_, err := Conn.Exec(context.Background(), "INSERT INTO free_tickets(user_id, free_ticket_id, organization_id) values($1, $2, $3)", userId, generated, organizationId)
	CheckErr(err)

	if searchTimeTicket(userId) {
		_, err = Conn.Exec(context.Background(), "UPDATE users SET organization_id=$2 " +
			"free_time=$3 WHERE user_id=$1", userId, organizationId, time.Now())
	} else {
		_, err = Conn.Exec(context.Background(), "INSERT INTO free_ticket_time(user_id, organization_id,free_time) values($1, $2, $3)", userId, organizationId, time.Now())
	}

	CheckErr(err)
	return generated
}

func searchTimeTicket(userId string) bool{
	rows, _ := Conn.Query(context.Background(), "SELECT * FROM free_ticket_time WHERE user_id=$1", userId)

	for rows.Next() {
		var a string
		err := rows.Scan(&a)
		if err != nil {
			return true
		}
	}
	return false
}

func checkCount(userId string, organizationId string)bool{
	var count int
	err := Conn.QueryRow(context.Background(),
		"SELECT count FROM users WHERE user_id=$1 AND organization_id=$2",
		userId, organizationId).Scan(&count)

	if err != nil {
		return false
	}


	if count >= countTickets {
		count = count - countTickets
		_, err = Conn.Exec(context.Background(),
			"UPDATE users SET count=$3 WHERE user_id=$1 AND organization_id=$2",
			userId, organizationId, count)
		return true
	}
	return false
}


func findFreeTicket(ticketReq TicketReq) bool{
	var ticketId string
	var freeTicketId string
	var organizationId string

	err := Conn.QueryRow(context.Background(),
		"SELECT * FROM free_tickets WHERE user_id=$1 AND organization_id=$2 LIMIT 1",
		ticketReq.DeviceId, ticketReq.OrganizationId).Scan(&ticketId, &freeTicketId, &organizationId)

	if err != nil {
		return false
	}
	return true
}




