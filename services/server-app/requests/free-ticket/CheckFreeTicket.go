package freeTicketRequests

import (
	"context"
	"encoding/json"
	"net/http"
)

type CheckFreeTicketReq struct {
	FreeTicketId string `json:"free_ticket_id"`
	ShowName     bool   `json:"show_name"`
}

type ResultCheckWithoutName struct {
	OrganizationId string `json:"organization_id"`
	DeviceId       string `json:"device_id"`
}

type ResultCheckWithName struct {
	OrganizationId   string `json:"organization_id"`
	OrganizationName string `json:"organization_name"`
	DeviceId         string `json:"device_id"`
}

func CheckFreeTicket(req *http.Request) (interface{}, error) {

	var reqData CheckFreeTicketReq
	var userId string
	var organizationId string
	var organizationName string

	err := json.NewDecoder(req.Body).Decode(&reqData)

	if err != nil {
		return nil, err
	}

	var query string

	if reqData.ShowName {

		query = `SELECT user_id, organizations.organization_id AS organization_id, organization_name
				FROM free_tickets, organizations 
				WHERE free_ticket_id=$1 AND free_tickets.organization_id=organizations.organization_id`
		row := Conn.QueryRow(context.Background(),
			query,
			reqData.FreeTicketId)
		err = row.Scan(&userId, &organizationId, &organizationName)
	} else {
		query = `SELECT user_id, organization_id
				FROM free_tickets WHERE free_ticket_id=$1`

		row := Conn.QueryRow(context.Background(),
			query,
			reqData.FreeTicketId)

		err = row.Scan(&userId, &organizationId)
	}


	if err != nil {
		return nil, err
	}

	var result interface{}

	if reqData.ShowName {
		result = ResultCheckWithName {
			OrganizationId:   organizationId,
			OrganizationName: organizationName,
			DeviceId:         userId,
		}
	} else {
		result = ResultCheckWithoutName{
			OrganizationId: organizationId,
			DeviceId: userId,
		}
	}
	return result, nil
}
