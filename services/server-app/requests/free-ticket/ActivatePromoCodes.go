package freeTicketRequests

import (
	"context"
	"encoding/json"
	"net/http"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)

type Data struct {
	Code string `json:"code"`
}

func ActivatePromoCode(req *http.Request) (interface{}, error) {
	var codeData Data

	err := json.NewDecoder(req.Body).Decode(&codeData)
	shared.CheckErr(err)

	if searchCode(codeData.Code) {
		_, err := shared.Conn.Exec(context.Background(),
			"DELETE  promo_codes WHERE code=$1", codeData.Code)
		shared.CheckErr(err)
		return shared.ErrorResult{"ActivationComplete"}, err
	} else {
		return shared.ErrorResult{"UncorrectableActivation"}, err
	}
}

func searchCode(code string) bool {

	rows, _ := shared.Conn.Query(context.Background(),
		"SELECT * FROM promo_codes WHERE code=$1", code)

	for rows.Next() {
		var dataInDb shared.PromoCode

		err := rows.Scan(&dataInDb.Type, &dataInDb.Code, &dataInDb.Mount, &dataInDb.ValidTime, &dataInDb.OrganizationId)

		if code == dataInDb.Code {
			return true
		}
		shared.CheckErr(err)
	}
	return false
}
