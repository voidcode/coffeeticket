package freeTicketRequests

import (
	"context"
	"encoding/json"
	"net/http"
)

const countTickets = 5


type UseTicketReq struct {
	FreeTicketId string `json:"free_ticket_id"`
}



func UseTicket(req *http.Request) (interface{}, error) {
	var body UseTicketReq

	err := json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		return nil, err
	}

	freeTicketId := body.FreeTicketId

	_, err = Conn.Exec(context.Background(),
		"DELETE FROM free_tickets WHERE free_ticket_id=$1", freeTicketId)
	
	if err != nil {
		return nil, err
	}



	result := ResultData{ Result: true }
	return result, nil
}
