package place

import (
	"context"
	"encoding/json"
	"net/http"
    "strings"
)

type QueryPlace struct {
	DeviceId string `json:"device_id"`
}

type QueryPlaceOrg struct {
	DeviceId string `json:"device_id"`
	OrganizationId string `json:"organization_id"`
}



var FreeCount = 5

func PlaceRequest(req *http.Request) (interface {}, error) {
        path := req.URL.Path
	
	splittedPath := strings.Split(path, "/")	
	placeId := splittedPath[len(splittedPath) - 1]

	var hasUserData bool
	var userId string
	var count int
	var organizationId string
	var title string
	var description string
	var location string
	var logoUrl string
	var reqData QueryPlace

	hasUserData = false


	row := Conn.QueryRow(context.Background(),
		"SELECT place_id, organization_id, title, description, location, logo_url FROM places WHERE place_id=$1", placeId)

	err := row.Scan(&placeId, &organizationId, &title, &description, &location, &logoUrl)

	if err != nil {
		return nil, err
	}

	// get count
	err = json.NewDecoder(req.Body).Decode(&reqData)
	// has body
	if err == nil {

		row := Conn.QueryRow(context.Background(),
			"SELECT user_id, organization_id, count FROM users WHERE user_id=$1 AND organization_id=$2",
			reqData.DeviceId,
			organizationId)

		err = row.Scan(&userId,
			&organizationId,
			&count)

		hasUserData = true
	}

	// check free-ticket
	query := `SELECT free_ticket_id 
				FROM free_tickets WHERE user_id=$1 AND organization_id=$2`

	row = Conn.QueryRow(context.Background(),
		query,
		userId, organizationId)

	var free_ticket_id string

	err = row.Scan(&free_ticket_id)

	if err == nil {
		count = FreeCount
	}


	if hasUserData {
		result := PlaceDataBody {
			Count: count,
			FreeCount: FreeCount,
			PlaceId: placeId,
			OrganizationId: organizationId,
			Title:          title,
			Description:    description,
			Location:       location,
			LogoUrl: logoUrl,
		}

		return result, nil
	} else {
		result := PlaceData{PlaceId: placeId,
							OrganizationId: organizationId,
							Title:          title,
							Description:    description,
							Location:       location,
							LogoUrl: logoUrl,
		}

		return result, nil
	}
}


