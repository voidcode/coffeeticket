package place

import (
	"context"
	"net/http"
)

func ListPlaces(_ *http.Request) (interface{}, error) {
	if ConErr != nil {
		return nil, ConErr
	}

	query := `SELECT place_id, organization_id, title, description, location, logo_url FROM places`

	rows, err := Conn.Query(context.Background(),
		query)

	if err != nil {
		return nil, err
	}

	var result []PlaceData

	for rows.Next() {

		var organizationId string
		var title string
		var description string
		var location string
		var placeId string
		var logoUrl string

		err = rows.Scan(&placeId, &organizationId, &title, &description, &location, &logoUrl)

		if err != nil {
			return nil, err
		}

		var entry = PlaceData {
			PlaceId: placeId,
			OrganizationId: organizationId,
			Title:          title,
			Description:    description,
			Location:       location,
			LogoUrl: logoUrl,
		}

		result = append(result, entry)
	}

	return result, nil
}
