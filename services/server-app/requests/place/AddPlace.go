package place

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
)


func AddPlace(req *http.Request) (interface{}, error) {
	var reqData PlaceData
	placeId := RandID(16)

	err := json.NewDecoder(req.Body).Decode(&reqData)

	if err != nil ||
		reqData.OrganizationId == "" ||
		reqData.Location == "" {
		return nil, errors.New("invalid data")
	}

	query := `INSERT INTO places(place_id,
								organization_id,
								title,
								description,
								location,
								logo_url
				) values($1, $2, $3, $4, $5, $6)`

	_, err = Conn.Exec(context.Background(), query, placeId,
		reqData.OrganizationId,
		reqData.Title,
		reqData.Description,
		reqData.Location,
		reqData.LogoUrl)
	if err != nil {
		return nil, err
	}

	return ResultData { Result: true }, nil

}
