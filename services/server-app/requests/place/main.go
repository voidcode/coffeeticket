package place

import (
	"voidcode.ru/bonusticket/packages/service-shared"
)

var Conn = service_shared.Conn
var ConErr = service_shared.ConErr
var RandID = service_shared.GenerateToken
type ResultData = service_shared.BoolResult


type PlaceData struct {
	PlaceId string `json:"place_id"`
	OrganizationId string `json:"organization_id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Location string `json:"location"`
	LogoUrl string `json:"logo_url"`
}

type PlaceDataBody struct {
	Count int `json:"count"`
	FreeCount int `json:"free_count"`
	LogoUrl string `json:"logo_url"`
	PlaceId string `json:"place_id"`
	OrganizationId string `json:"organization_id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Location string `json:"location"`
}