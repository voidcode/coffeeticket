package place

import (
	"context"
	"encoding/json"
	"net/http"
)

type Organization struct {
	OrganizationName string `json:"organization_name"`
	OrganizationId string `json:"organization_id"`
}


func AddOrganization(req *http.Request) (interface{}, error) {
	var reqData Organization

	err := json.NewDecoder(req.Body).Decode(&reqData)

	if (err != nil && reqData.OrganizationName != "") {
		return nil, err
	}

	organizationName := reqData.OrganizationName
	organizationId := reqData.OrganizationId
	if organizationId == "" {
		organizationId = RandID(16)
	}

	query := `INSERT INTO organizations(organization_id, organization_name) values($1, $2)`

	_, err = Conn.Exec(context.Background(), query,
		organizationId,
		organizationName)
	if err != nil {
		return nil, err
	}

	return ResultData { Result: true }, nil

}
