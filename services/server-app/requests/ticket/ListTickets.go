package ticket

import (
	"context"
	"net/http"
)

type ListRequestData struct{
	TicketId       string `json:"ticket_id"`
	OrganizationId string `json:"organization_id"`
}
func ListTickets(_ *http.Request) (interface{}, error){
	rows, err := Conn.Query(context.Background(), "SELECT * FROM tickets")

	if err != nil {
		return nil, err
	}

	var list []ListRequestData

	for rows.Next() {
		var responseData ListRequestData
		var ticketId string
		var organizationId string
		err := rows.Scan(&ticketId, &organizationId)

		if err != nil {
			return nil, err
		}

		responseData.OrganizationId = organizationId
		responseData.TicketId = ticketId

		list = append(list, responseData)
	}



	return list, nil

}
