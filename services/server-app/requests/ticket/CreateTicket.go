package ticket

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
)

type CreateIdReqData struct {
	OrganizationId string `json:"organization_id"`
	Count int `json:"count"`
}

func CreateTicket(req *http.Request) (interface{}, error) {
	var params CreateIdReqData
	err := json.NewDecoder(req.Body).Decode(&params)
	if err != nil {
		return nil, err
	}

	if params.Count == 0 || params.OrganizationId == "" {
		return nil, errors.New("invalid data")
	}


	for i := 0; i < params.Count; i++ {
		ticketId := RandID(16)

		_, err := Conn.Exec(context.Background(), "INSERT INTO tickets(ticket_id, organization_id) values($1, $2)", ticketId, params.OrganizationId)

		if err != nil {
			return nil, err
		}
	}

	result := ResultData{ Result: true }
	return result, nil
}
