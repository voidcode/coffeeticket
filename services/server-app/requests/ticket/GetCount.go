package ticket

import (
	"context"
	"encoding/json"
	"net/http"
)

type CountReq struct{
	DeviceId       string `json:"device_id"`
	OrganizationId string `json:"organization_id"`
}

type CountResData struct {
	Count int `json:"count"`
}


func GetCount(req *http.Request) (interface{}, error) {

	var ticketReq CountReq
	err := json.NewDecoder(req.Body).Decode(&ticketReq)
	if err != nil {
		return nil, err
	}

	var count int
	err = Conn.QueryRow(context.Background(),
		"SELECT count FROM users WHERE user_id=$1 AND organization_id=$2",
		ticketReq.DeviceId,
		ticketReq.OrganizationId).Scan(&count)

	if err != nil {
		return nil, err
	}

	result := CountResData{ Count: count }

	return result, nil

}