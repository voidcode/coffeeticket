package ticket

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

type QrReqData struct {
	TicketId string `json:"ticket_id"`
	DeviceId string `json:"device_id"`
}

type QrResData struct {
	Result bool `json:"result"`
}

func QrQuery(req *http.Request) (interface{}, error) {
	var id_ QrReqData

	err := json.NewDecoder(req.Body).Decode(&id_)

	if err != nil {
		return nil, err
	}

	if searchId(id_.TicketId) && id_.DeviceId != "" {
		organizationId := getOrganizationByTicketId(id_.TicketId)
		deleteId(id_.TicketId)

		var userId string
		var count int

		err = Conn.QueryRow(context.Background(),
			"SELECT * FROM users WHERE user_id=$1 AND organization_id=$2 LIMIT 1",
			id_.DeviceId, organizationId).Scan(&userId, &organizationId, &count)

		var doesUserExist bool

		if err != nil {
			fmt.Println(err)
			// Does not exist
			doesUserExist = false
			count = 0
		} else {
			doesUserExist = true
		}

		count++

		var query string
		if doesUserExist {
			query = "UPDATE users SET count=$3 WHERE user_id=$1 AND organization_id=$2"
		} else {
			query = "INSERT INTO users(user_id, organization_id, count) values($1, $2, $3)"
		}

		if searchRecent(id_.DeviceId, organizationId) {

			_, err = Conn.Exec(context.Background(),
				"UPDATE recent SET last_time=$3 WHERE user_id=$1 AND organization_id=$2",
				id_.DeviceId, organizationId, time.Now())
		} else {
			_, err = Conn.Exec(context.Background(),
				"INSERT INTO recent(user_id, organization_id, last_time) values($1, $2, $3)",
				id_.DeviceId, organizationId, time.Now())
		}

		if err != nil {
			return nil, err
		}

		_, err = Conn.Exec(context.Background(),
			query, id_.DeviceId, organizationId, count)

		if err != nil {
			return nil, err
		}

		result := ResultData{ Result: true }
		return result, nil

	} else {
		return nil, errors.New("no such ticket")
	}
}

func getOrganizationByTicketId(id string) string {
	rows, err := Conn.Query(context.Background(), "SELECT * FROM tickets WHERE ticket_id=$1", id)
	CheckErr(err)

	for rows.Next() {
		var ticketId string
		var organizationId string

		err := rows.Scan(&ticketId, &organizationId)
		CheckErr(err)

		return organizationId
	}

	panic("No organization")
}

func deleteId(id string) {
	_, err := Conn.Exec(context.Background(), "DELETE FROM tickets WHERE ticket_id=$1", id)
	CheckErr(err)
}

func searchRecent(userId string, organizationId string) bool {

	rows, _ := Conn.Query(context.Background(), "SELECT * FROM recent WHERE user_id=$1 AND organization_id=$2", userId, organizationId)

	for rows.Next() {
		var a string
		err := rows.Scan(&a)
		if err != nil {
			return true
		}
	}
	return false
}

func searchId(id string) bool {
	rows, _ := Conn.Query(context.Background(), "SELECT * FROM tickets WHERE ticket_id=$1", id)

	for rows.Next() {
		var a string
		err := rows.Scan(&a)
		if err != nil {
			return true
		}
	}
	return false

}
