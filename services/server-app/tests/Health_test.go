package tests

import (
	"testing"
	"voidcode.ru/bonusticket/packages/service-shared"
	"voidcode.ru/bonusticket/services/server-app/requests/health"
)

func TestHealth(t *testing.T) {
	service_shared.TestGetRequest(t, "/", health.Health, `{"result":"OK"}`)
}