package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"
	shared "voidcode.ru/bonusticket/packages/service-shared"
	"voidcode.ru/bonusticket/services/server-app/requests/free-ticket"
	"voidcode.ru/bonusticket/services/server-app/requests/health"
	"voidcode.ru/bonusticket/services/server-app/requests/place"
	"voidcode.ru/bonusticket/services/server-app/requests/ticket"
)

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

var schema = `

	CREATE TABLE IF NOT EXISTS tickets ();
	ALTER TABLE tickets ADD IF NOT EXISTS ticket_id text;
	ALTER TABLE tickets ADD IF NOT EXISTS organization_id text;

	CREATE TABLE IF NOT EXISTS users ();
	ALTER TABLE users ADD IF NOT EXISTS user_id text;
	ALTER TABLE users ADD IF NOT EXISTS organization_id text;
	ALTER TABLE users ADD IF NOT EXISTS count int;

	CREATE TABLE IF NOT NOT EXIST sales ();
	ALTER TABLE sales IF NOT EXIST sale_id text;
	ALTER TABLE sales IF NOT EXIST user_id text;
	ALTER TABLE sales IF NOT EXIST percent int;
	ALTER TABLE sales IF NOT EXIST organization_id text;
	ALTER TABLE sales IF NOT EXIST valid_time timestamp;

	CREATE TABLE IF NOT NOT EXIST promo_codes ();
	ALTER TABLE promo_codes IF NOT EXIST type text;
	ALTER TABLE promo_codes IF NOT EXIST code text;
	ALTER TABLE promo_codes IF NOT EXIST valid_time timestamp;
	ALTER TABLE promo_codes IF NOT EXIST mount text;
	ALTER TABLE promo_codes IF NOT EXIST organization_id text;

	CREATE TABLE IF NOT EXISTS free_tickets ();
	ALTER TABLE free_tickets ADD IF NOT EXISTS user_id text;
	ALTER TABLE free_tickets ADD IF NOT EXISTS free_ticket_id text;
	ALTER TABLE free_tickets ADD IF NOT EXISTS organization_id text;

	CREATE TABLE IF NOT EXISTS places ();
	ALTER TABLE places ADD IF NOT EXISTS place_id text;
	ALTER TABLE places ADD IF NOT EXISTS organization_id text;
	ALTER TABLE places ADD IF NOT EXISTS title text;
	ALTER TABLE places ADD IF NOT EXISTS description text;
	ALTER TABLE places ADD IF NOT EXISTS location text;
	ALTER TABLE places ADD IF NOT EXISTS logo_url text;

	CREATE TABLE IF NOT EXISTS free_ticket_time ();
	ALTER TABLE free_ticket_time ADD IF NOT EXISTS user_id text;
	ALTER TABLE free_ticket_time ADD IF NOT EXISTS organization_id text;
	ALTER TABLE free_ticket_time ADD IF NOT EXISTS free_time timestamp;

	CREATE TABLE IF NOT EXISTS recent ();
	ALTER TABLE recent ADD IF NOT EXISTS user_id text;
	ALTER TABLE recent ADD IF NOT EXISTS organization_id text;
	ALTER TABLE recent ADD IF NOT EXISTS last_time timestamp;

	CREATE TABLE IF NOT EXISTS organizations ();
	ALTER TABLE organizations ADD IF NOT EXISTS organization_id text;
	ALTER TABLE organizations ADD IF NOT EXISTS organization_name text;
`

func main() {
	shared.DBDoctor(schema)

	var Handler = shared.RequestHandler

	var prefix = os.Getenv("PREFIX")

	fmt.Println("Serving at " + "URL" + prefix + "/query-name")

	http.HandleFunc(prefix+"/activatepromocode", Handler(freeTicketRequests.ActivatePromoCode))
	http.HandleFunc(prefix+"/getusersales", Handler(freeTicketRequests.GetUserSales))

	http.HandleFunc(prefix+"/places", Handler(place.ListPlaces))
	http.HandleFunc(prefix+"/places/", Handler(place.PlaceRequest))
	http.HandleFunc(prefix+"/addPlace", Handler(place.AddPlace))
	http.HandleFunc(prefix+"/addOrganization", Handler(place.AddOrganization))

	http.HandleFunc(prefix+"/listTickets", Handler(ticket.ListTickets))
	http.HandleFunc(prefix+"/qrQuery", Handler(ticket.QrQuery))
	http.HandleFunc(prefix+"/createId", Handler(ticket.CreateTicket))

	http.HandleFunc(prefix+"/headers", headers)
	http.HandleFunc(prefix+"/", Handler(health.Health))
	http.HandleFunc(prefix+"/useTicket", Handler(freeTicketRequests.UseTicket))
	http.HandleFunc(prefix+"/checkTicket", Handler(freeTicketRequests.CheckFreeTicket))
	http.HandleFunc(prefix+"/getFreeTicket", Handler(freeTicketRequests.GetFreeTicket))

	http.HandleFunc("/getCount", Handler(ticket.GetCount))

	portStr := os.Getenv("PORT")
	fmt.Println("Listening on " + portStr)
	err := http.ListenAndServe(":"+portStr, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())
}
