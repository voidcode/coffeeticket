package requests

import (
	"context"
	"encoding/json"
	"net/http"
	service_shared "voidcode.ru/bonusticket/packages/service-shared"
	//"time"
	shared "voidcode.ru/bonusticket/packages/service-shared"
	"voidcode.ru/bonusticket/services/server-app/requests/place"
)

type ChangePlaceData struct {
	place.PlaceDataBody
	Token string `json:"token"`
}

func Changeinfo(req *http.Request) (interface{}, error) {
	var reqData ChangePlaceData
	var helpData place.PlaceDataBody

	err := json.NewDecoder(req.Body).Decode(reqData)
	service_shared.CheckErr(err)

	if SearchToken(reqData.Token) {
		if searchPlaceData(reqData) {
			row := shared.Conn.QueryRow(context.Background(),
				"SELECT * FROM places WHERE place_id=$1", reqData.PlaceId)
			err = row.Scan(&helpData.PlaceId, &helpData.OrganizationId, &helpData.Title, &reqData.Description, &reqData.Location, &reqData.LogoUrl)
			shared.CheckErr(err)

			if reqData.Title == "" {
				reqData.Title = helpData.Title
			}
			if reqData.Description == "" {
				reqData.Description = helpData.Description
			}
			if reqData.Location == "" {
				reqData.Location = helpData.Location
			}
			if reqData.LogoUrl == "" {
				reqData.LogoUrl = helpData.LogoUrl
			}
			_, err = shared.Conn.Exec(context.Background(),
				"UPDATE places SET title=$1 AND description=$2 AND location=$3 AND logo_url=$4 WHERE place_id=$5",
				reqData.Title, reqData.Description, reqData.Location, reqData.LogoUrl, reqData.PlaceId)
			shared.CheckErr(err)

			return shared.ErrorResult{Error: "Complete"}, err
		}
		return shared.ErrorResult{Error: "Incorrect PlaceId."}, err
	} else {
		return shared.ErrorResult{Error: "Authorization failed"}, err
	}
}

func searchPlaceData(reqData ChangePlaceData) bool {

	rows, _ := shared.Conn.Query(context.Background(),
		"SELECT * FROM places WHERE place_id=$1", reqData.PlaceId)

	for rows.Next() {
		var PlaceId string

		err := rows.Scan(&PlaceId)

		if PlaceId == reqData.PlaceId {
			return true
		}
		shared.CheckErr(err)
	}
	return false
}
