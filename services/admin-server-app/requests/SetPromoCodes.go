package requests

import (
	"context"
	"encoding/json"
	"net/http"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)

const PROMO_CODE_LENGTH = 8

type ChangePromoCode struct {
	shared.PromoCode
	Token string `json:"token"`
}

func SetPromoCode(req *http.Request) (interface{}, error) {
	var promoCodeAdd shared.PromoCode
	var ChangeCode ChangePromoCode
	err := json.NewDecoder(req.Body).Decode(&ChangeCode)
	shared.CheckErr(err)

	if SearchToken(ChangeCode.Token) {
		promoCodeAdd = ChangeCode.PromoCode

		for i := 0; i < promoCodeAdd.Mount; i++ {
			_, err = shared.Conn.Exec(context.Background(),
				"INSERT INTO promo_codes(type=$1, code=$2, valid_time=$3, mount=$4, organization_id=$5)",
				promoCodeAdd.Type, shared.GenerateToken(PROMO_CODE_LENGTH), promoCodeAdd.ValidTime, promoCodeAdd.Mount, promoCodeAdd.OrganizationId)
			shared.CheckErr(err)
		}

		return shared.ErrorResult{Error: "Complete"}, err
	} else {
		return shared.ErrorResult{Error: "Authorization failed."}, err
	}
}
