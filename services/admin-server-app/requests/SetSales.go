package requests

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)

type Sale struct {
	SaleId         string    `json:"sale_id"`
	UserId         string    `json:"user_id"`
	Percent        string    `json:"percent"`
	OrganizationId string    `json:"organization_id"`
	ValidTime      time.Time `json:"valid_time"`
}

type ChangeSale struct {
	Sale
	Token string `json:"token"`
}

func SetSales(req *http.Request) (interface{}, error) {
	var SaleAdd Sale
	var AuthSale ChangeSale

	err := json.NewDecoder(req.Body).Decode(&AuthSale)
	shared.CheckErr(err)

	SaleAdd = AuthSale.Sale

	if SearchToken(AuthSale.Token) {

		_, err = shared.Conn.Exec(context.Background(),
			"INSERT INTO sales(sale_id=$1, user_id=$2, percent=$3, organization_id=$4, valid_time=$5)",
			SaleAdd.SaleId, SaleAdd.UserId, SaleAdd.Percent, SaleAdd.OrganizationId, SaleAdd.ValidTime)
		shared.CheckErr(err)
		return shared.ErrorResult{Error: "Complete"}, err
	} else {
		return shared.ErrorResult{Error: "Auth failed"}, err
	}
}
