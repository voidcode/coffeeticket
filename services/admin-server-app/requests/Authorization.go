package requests

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	//"testing/quick"
	"time"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)



const ticketLength = 16

type AuthData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Token string `json:"token"`
}

type TokenResult struct {
	Token string `json:"token"`
}

func Authorization(req *http.Request) (interface{}, error) {
	var reqData AuthData
	var UserData UserForm

	err := json.NewDecoder(req.Body).Decode(&reqData)

	UserData.Password = reqData.Password
	UserData.Email = reqData.Email

	if err != nil {
		return nil, err
	}



	if SearchToken(reqData.Token) {
		if timeChecker(reqData.Token) {
			return TokenResult{reqData.Token}, err
		} else {
			return shared.ErrorResult{Error: "Token Is Not Valid"}, err
		}
	} else if SearchEnterInDB(UserData) {
		tokenGenerated := shared.GenerateToken(ticketLength)

		fmt.Println(reqData)
		fmt.Println("Token is", tokenGenerated)
		fmt.Println("SearchUser", searchUser(reqData))
		_, err = shared.ConnAdmin.Exec(context.Background(), "INSERT INTO users_auth(email, token, valid_time) values($1, $2, $3)", reqData.Email, tokenGenerated, time.Now())
		fmt.Println(err, "\n", tokenGenerated, time.Now(), reqData.Email)
		// if !searchUser(reqData) {
		// 	_, err = shared.ConnAdmin.Exec(context.Background(), "INSERT INTO users_auth(email, token, valid_time) values($1, $2, $3)", reqData.Email, tokenGenerated, time.Now())

		// 	fmt.Println(err, "\n", tokenGenerated, time.Now(), reqData.Email)
		// } else {
		// 	_, err = shared.ConnAdmin.Exec(context.Background(), "UPDATE users_auth SET token=$2 AND valid_time=$3 WHERE email=$1", reqData.Email, tokenGenerated, time.Now())
		// 	fmt.Println(err, "\n", tokenGenerated, time.Now(), reqData.Email)
		// }
		shared.CheckErr(err)
		return TokenResult{tokenGenerated}, err
	} else {
		return shared.ErrorResult{Error: "Incorrect user or password."}, err
	}
}

func timeChecker(token string) bool {
	var email string
	var password string
	var tokenInDb string
	var timeInDb time.Time

	if SearchToken(token) {
		row := shared.ConnAdmin.QueryRow(context.Background(),
			"SELECT * FROM users_auth WHERE token=$1", token)
		row.Scan(&email, &password, &tokenInDb, &timeInDb)

		return passageTime(timeInDb)
	}
	return false
}

func passageTime(DBtime time.Time) bool {
	if DBtime.Before(time.Now()) {
		check := time.Now().AddDate(0, 0, 7)
		if DBtime.After(check) {
			return false
		}
	}
	return true
}

func searchUser(reqData AuthData) bool {

	rows, err := shared.ConnAdmin.Query(context.Background(),
		"SELECT * FROM users_auth WHERE email=$1", reqData.Email)

	shared.CheckErr(err)

	for rows.Next() {
		var email string
		var token string
		var timeDb time.Time

		err := rows.Scan(&email, &token, &timeDb)

		if email == reqData.Email {
			return true
		}
		shared.CheckErr(err)
	}
	return false
}

func SearchToken(token string) bool {


	rows, err := shared.ConnAdmin.Query(context.Background(),
		"SELECT * FROM users_auth WHERE token=$1", token)
	shared.CheckErr(err)

	for rows.Next() {
		var email string
		var password string
		var tokenInDb string

		err := rows.Scan(&email, &password, &tokenInDb)

		shared.CheckErr(err)

		if token == tokenInDb {
			return true
		} else {
			return false
		}
	}
	return false
}
