package requests

import (
	"context"
	"encoding/json"
	"net/http"
	shared "voidcode.ru/bonusticket/packages/service-shared"
	"voidcode.ru/bonusticket/services/server-app/requests/place"
)

type OrganizationId struct {
	OrganizationId string `json:"organization_id"`
}

func GetPlacesByOrganizationId(req *http.Request) (interface{}, error) {
	var OrganizationId OrganizationId

	err := json.NewDecoder(req.Body).Decode(&OrganizationId)
	shared.CheckErr(err)

	entries := searchPlaceByOrganizationId(OrganizationId.OrganizationId)

	if entries != nil {
		return entries, err
	} else {
		return shared.ErrorResult{"Organizations_Not_Found"}, err
	}
}

func searchPlaceByOrganizationId(OrgId string) []place.PlaceData {

	rows, _ := shared.Conn.Query(context.Background(),
		"SELECT * FROM places WHERE place_id=$1", OrgId)

	var Places []place.PlaceData

	for rows.Next() {
		var Place place.PlaceData

		err := rows.Scan(&Place.PlaceId, &Place.OrganizationId, &Place.Title, &Place.Description, &Place.Location, &Place.LogoUrl)

		if Place.PlaceId == OrgId {
			Places = append(Places, Place)
		}
		shared.CheckErr(err)
	}
	return Places
}
