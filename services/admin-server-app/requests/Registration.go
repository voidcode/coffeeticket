package requests

import (
	"context"
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	shared "voidcode.ru/bonusticket/packages/service-shared"
)

type UserForm struct{
	Email string `json:"Email"`
	Password string `json:"password"`
}

func Registration(req  *http.Request) (interface{}, error){
	var reqData UserForm

	err := json.NewDecoder(req.Body).Decode(&reqData)

	if err != nil{
		return nil, err
	}

	if SearchEnterInDB(reqData) == false{
		_, err = shared.ConnAdmin.Exec(context.Background(), "INSERT INTO Users(email, password) values($1, $2)", reqData.Email, reqData.Password)
		shared.CheckErr(err)
		result := shared.BoolResult{Result: true}
		return result, err
	} else {
		result := shared.ErrorResult {Error: "User already exists." }
		return result, err
	}
}

func SearchEnterInDB(reqData UserForm) bool{

	rows, _ := shared.ConnAdmin.Query(context.Background(),
		"SELECT * FROM users WHERE email=$1", reqData.Email)

	for rows.Next() {
		var email string
		var password string

		reqPassword, err := bcrypt.GenerateFromPassword([]byte(reqData.Password), bcrypt.DefaultCost)

		err = rows.Scan(&email, &password)

		if string(reqPassword) == password{
			return true
		}else if email == reqData.Email{
			return true
		}
		if err != nil {
			return false
		}
	}
	return false
}
