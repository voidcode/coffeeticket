package main

import (
	"fmt"
	"net/http"
	"os"
	shared "voidcode.ru/bonusticket/packages/service-shared"
	"voidcode.ru/bonusticket/services/admin-server-app/requests"
)

var schema = `
	CREATE TABLE IF NOT EXISTS users ();
	ALTER TABLE users ADD IF NOT EXISTS email text;
	ALTER TABLE users ADD IF NOT EXISTS password text;

	CREATE TABLE IF NOT EXISTS users_auth ();
	ALTER TABLE users_auth ADD IF NOT EXISTS email text;
	ALTER TABLE users_auth ADD IF NOT EXISTS token text;
	ALTER TABLE users_auth ADD IF NOT EXISTS valid_time timestamp;
`

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func main() {
	shared.DBDoctor(schema)

	var Handler = shared.RequestHandler

	var prefix = os.Getenv("PREFIX")

	http.HandleFunc(prefix+"/addpromocode", Handler(requests.SetPromoCode))
	http.HandleFunc(prefix+"/addsale", Handler(requests.SetSales))

	http.HandleFunc(prefix+"/changeinfo", Handler(requests.Changeinfo))
	http.HandleFunc(prefix+"/registration", Handler(requests.Registration))
	http.HandleFunc(prefix+"/authorization", Handler(requests.Authorization))
	http.HandleFunc(prefix+"/getplacesbyorganizationid", Handler(requests.GetPlacesByOrganizationId))
	http.HandleFunc(prefix+"/headers", headers)

	fmt.Println("Serving at " + "URL" + prefix + "/query-name")
	portStr := os.Getenv("PORT")
	fmt.Println("Listening on " + portStr)
	err := http.ListenAndServe(":"+portStr, nil)
	checkErr(err)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
